#ifndef SEARCH_AND_XML_SYNTAX_HIGHLIGHTER_HPP
#define SEARCH_AND_XML_SYNTAX_HIGHLIGHTER_HPP

#include "search_highlighter.hpp"

class SearchAndXmlSyntaxHighlighter
    : public SearchHighlighter
{
  public:
    SearchAndXmlSyntaxHighlighter(QTextDocument* document = nullptr);

    enum SyntaxMode
    {
      kTxt = 0,
      kXml
    };

    void SetSyntaxMode(SyntaxMode mode);
  protected:
    void highlightBlock(const QString & text) override;

  private:
    void HighlightByRegex(const QColor & color,
                          const QRegularExpression & regex, const QString & text);

    void SetupRegexes();
    void SetupColors();

    SyntaxMode syntax_mode_;
    QColor tag_color_;
    QColor attribute_color_;
    QColor value_color_;

    QRegularExpression close_bracket_regex_;
    QRegularExpression open_tag_regex_;
    QRegularExpression close_tag_regex_;
    QRegularExpression self_closing_tag_;
    QRegularExpression attribute_regex_;
    QRegularExpression value_regex_;
    QRegularExpression prolog_regex_;
};

#endif // SEARCH_AND_XML_SYNTAX_HIGHLIGHTER_HPP
