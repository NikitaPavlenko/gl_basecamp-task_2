#include "text_editor.hpp"

#include "line_numbers_area.hpp"
#include <QDomDocument>

TextEditor::TextEditor(QWidget* parent)
  : QPlainTextEdit(parent)
  , line_numbers_area_(new LineNumbersArea(this))
  , text_searcher_(new SearchAndXmlSyntaxHighlighter(document()))
  , xml_validator_(new QDomDocument)
{
  setLineWrapMode(LineWrapMode::NoWrap);

  connect(this, &QPlainTextEdit::blockCountChanged,
          this, &TextEditor::slot_UpdateLineNumbersAreaWidth);
  connect(this, &QPlainTextEdit::updateRequest,
          this, &TextEditor::slot_UpdateLineNumbersArea);
}

TextEditor::~TextEditor()
{

}

void TextEditor::ShowLineNumbers()
{
  line_numbers_area_->show();
  slot_UpdateLineNumbersAreaWidth(0);
}

void TextEditor::HideLineNumbers()
{
  line_numbers_area_->hide();
  slot_UpdateLineNumbersAreaWidth(0);
}

bool TextEditor::IsLineNumbersVisible()
{
  return line_numbers_area_->isVisible();
}

void TextEditor::SearchText(const QString& pattern)
{
   text_searcher_->Search(pattern);
}

void TextEditor::SetSyntaxMode(SearchAndXmlSyntaxHighlighter::SyntaxMode mode)
{
  text_searcher_->SetSyntaxMode(mode);
}

bool TextEditor::ValidateLikeXmlText()
{
  bool result = xml_validator_->setContent(document()->toRawText());
  xml_validator_->clear();
  return result;
}

void TextEditor::slot_UpdateLineNumbersArea(const QRect &rect, int dy)
{
  if (dy)
    line_numbers_area_->scroll(0, dy);
  else
    line_numbers_area_->update(0, rect.y(), line_numbers_area_->width(), rect.height());

  if (rect.contains(viewport()->rect()))
    slot_UpdateLineNumbersAreaWidth(0);
}

void TextEditor::resizeEvent(QResizeEvent *e)
{
  QPlainTextEdit::resizeEvent(e);

  QRect cr = contentsRect();
  line_numbers_area_->setGeometry(QRect(cr.left(), cr.top(), line_numbers_area_->UpdateWidth(), cr.height()));
}

void TextEditor::slot_UpdateLineNumbersAreaWidth(int new_block_count)
{
  Q_UNUSED(new_block_count)
  setViewportMargins(line_numbers_area_->UpdateWidth(), 0, 0, 0);
}
