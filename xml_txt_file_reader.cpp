#include "xml_txt_file_reader.hpp"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextDocument>
#include <QTextCursor>

XmlTxtFileReader::XmlTxtFileReader()
  : ReadService()
{

}

QString XmlTxtFileReader::Read(QTextDocument& document)
{
  QString file_name = QFileDialog::getOpenFileName(nullptr, "Open file", "",
                                                   "TXT files (*.txt);;XML files (*.xml)");
  if (file_name.isEmpty())
    return QString();
  else
  {
    QFile file(file_name);
    if (!file.open(QIODevice::ReadOnly))
    {
      QMessageBox::information(nullptr, "Unable to read file", file.errorString());
      return QString();
    }
    document.clear();
    QTextCursor cursor(&document);
    cursor.insertText(QString::fromUtf8(file.readAll()));
    file.close();
    return file_name;
  }
}
