#include "line_numbers_area.hpp"
#include "text_editor.hpp"

#include <QTextBlock>
#include <QPainter>

LineNumbersArea::LineNumbersArea(TextEditor *editor)
  : QWidget(editor)
  , editor_(editor)
  , left_padding_(5)
  , right_padding_(0)
{
}

QSize LineNumbersArea::sizeHint() const
{
  return QSize(UpdateWidth(), 0);
}

int LineNumbersArea::UpdateWidth() const
{
  if (isHidden() || !editor_->isEnabled())
    return 0;

  int digits = 1;
  int max = qMax(1, editor_->blockCount());
  while (max >= 10)
  {
    max /= 10;
    ++digits;
  }
  return left_padding_ +
      right_padding_ +
      fontMetrics().horizontalAdvance(QLatin1Char('9')) * digits;
}

void LineNumbersArea::paintEvent(QPaintEvent* event)
{
  if (isHidden() || !editor_->isEnabled())
    return;

  QPainter painter(this);
  painter.fillRect(event->rect(), Qt::lightGray);

  QTextBlock block = editor_->firstVisibleBlock();
  int block_number = block.blockNumber() + 1;
  int top = qRound(editor_->blockBoundingGeometry(block).translated(editor_->contentOffset()).top());
  int bottom = top + qRound(editor_->blockBoundingRect(block).height());

  painter.setPen(Qt::black);
  while (block.isValid() && top <= event->rect().bottom())
  {
    if (block.isVisible() && bottom >= event->rect().top())
      painter.drawText(left_padding_, top, width() - right_padding_ - left_padding_, fontMetrics().height(),
                       Qt::AlignRight, QString::number(block_number));

    block = block.next();
    top = bottom;
    bottom = top + qRound(editor_->blockBoundingRect(block).height());
    ++block_number;
  }
}
