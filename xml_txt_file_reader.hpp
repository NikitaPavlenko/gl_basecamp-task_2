#ifndef XML_TXT_FILE_READER_H
#define XML_TXT_FILE_READER_H

#include "read_service.hpp"

class QTextDocument;

class XmlTxtFileReader : public ReadService<QTextDocument>
{
  public:
    XmlTxtFileReader();
    ~XmlTxtFileReader() override {}

    QString Read(QTextDocument& document) override;
};

#endif // XML_TXT_FILE_READER_H
