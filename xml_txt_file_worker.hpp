#ifndef XML_TXT_FILE_WORKER_H
#define XML_TXT_FILE_WORKER_H

#include "file_service.hpp"

#include "read_service.hpp"
#include "write_service.hpp"

#include <QScopedPointer>

#include <QTextDocument>

class XmlTxtFileWorker : public FileService
{
  public:
    XmlTxtFileWorker(QTextDocument* document);
    ~XmlTxtFileWorker() override {}

    bool Create() override;
    bool Open() override;
    bool Save() override;
    bool SaveAs() override;
    bool Close() override;

  private:
    QTextDocument* document_;
    QScopedPointer<ReadService<QTextDocument>> reader_;
    QScopedPointer<WriteService<QTextDocument>> writer_;
    quint64 current_file_text_hash_;
};

#endif // XML_TXT_FILE_WORKER_H
