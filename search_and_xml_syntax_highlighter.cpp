#include "search_and_xml_syntax_highlighter.hpp"

SearchAndXmlSyntaxHighlighter::SearchAndXmlSyntaxHighlighter(QTextDocument* document)
  : SearchHighlighter(document)
  , syntax_mode_(kTxt)
{
  SetupRegexes();
  SetupColors();
}

void SearchAndXmlSyntaxHighlighter::SetSyntaxMode(SearchAndXmlSyntaxHighlighter::SyntaxMode mode)
{
  syntax_mode_ = mode;
}

void SearchAndXmlSyntaxHighlighter::highlightBlock(const QString& text)
{
  if (syntax_mode_ == SyntaxMode::kXml)
  {
    HighlightByRegex(tag_color_, close_bracket_regex_, text);
    HighlightByRegex(tag_color_, open_tag_regex_, text);
    HighlightByRegex(tag_color_, close_tag_regex_, text);
    HighlightByRegex(tag_color_, self_closing_tag_, text);
    HighlightByRegex(attribute_color_, attribute_regex_, text);
    HighlightByRegex(value_color_, value_regex_, text);
    HighlightByRegex(tag_color_, prolog_regex_, text);
  }

  SearchHighlighter::highlightBlock(text);
}

void SearchAndXmlSyntaxHighlighter::HighlightByRegex(const QColor& color,
                                                     const QRegularExpression& regex,
                                                     const QString& text)
{
  QRegularExpressionMatchIterator it = regex.globalMatch(text);
  while (it.hasNext())
  {
    QRegularExpressionMatch match = it.next();
    setFormat(match.capturedStart(), match.capturedLength(), color);
  }
}

void SearchAndXmlSyntaxHighlighter::SetupRegexes()
{
  open_tag_regex_.setPattern("<\\s*(?<tag>[^/<>\"'\\s]+)(?=(\\s+[^<>\\s]+)*\\s*(?<!/)\\s*>)");
  close_bracket_regex_.setPattern(">|/>");
  close_tag_regex_.setPattern("<\\s*/\\s*(?<tag>[^<\"'>\\s]+)>");
  self_closing_tag_.setPattern("<\\s*[^/<>'\"\\s]+(?=\\s*.*\\s*/\\s*>)");
  attribute_regex_.setPattern("(?<=\\s)*[^'\"<>\\s]+(?=\\s*=\\s*['\"])");
  value_regex_.setPattern("\"[^\"']*\"|'[^\"']*'\"");
  prolog_regex_.setPattern("<\\?.*\\?>");
}

void SearchAndXmlSyntaxHighlighter::SetupColors()
{
  tag_color_ = Qt::blue;
  attribute_color_ = Qt::red;
  value_color_ = Qt::darkGreen;
}
