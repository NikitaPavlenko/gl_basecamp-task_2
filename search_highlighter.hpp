#ifndef SEARCH_HIGHLIGHTER_HPP
#define SEARCH_HIGHLIGHTER_HPP

#include <QSyntaxHighlighter>
#include <QRegularExpression>

class SearchHighlighter
    : public QSyntaxHighlighter
{
    Q_OBJECT
  public:
    SearchHighlighter(QTextDocument* document);

    void Search(const QString& text);

    // QSyntaxHighlighter interface
  protected:
    void highlightBlock(const QString& text) override;

  private:
    QRegularExpression regex_;
    QColor background_color_;
    QColor foreground_color_;
};

#endif // SEARCH_HIGHLIGHTER_HPP
