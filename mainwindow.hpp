#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"

#include <QMainWindow>

#include "search_and_xml_syntax_highlighter.hpp"

class QLineEdit;
class TextStatisticCounter;
class FileService;

class MainWindow : public QMainWindow
{
    Q_OBJECT
  public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

  private slots:
    void slot_CreateTxtFile();
    void slot_CreateXmlFile();
    void slot_OpenFile();
    void slot_SaveFile();
    void slot_SaveAsFile();
    void slot_CloseFile();
    void slot_CutText();
    void slot_CopyText();
    void slot_PasteText();
    void slot_UndoAction();
    void slot_RedoAction();
    void slot_ShowHideLineNumbers();
    void slot_XmlFileValidate();
    void slot_SearchPatternChanged();
    void slot_WordCountChanged(quint32 new_count);
    void slot_CharacterCountChanged(quint32 new_count);
    void slot_ClearXmlValidateResult();

  private:
    void Setup();
    void SetEditorEnable();

    Ui::MainWindow ui;
    QLineEdit* search_line_edit_;
    QScopedPointer<TextStatisticCounter> statistic_counter_;
    QScopedPointer<FileService> file_worker_;
    QString invalid_xml_text_message_;
    QString valid_xml_text_message_;
};
#endif // MAINWINDOW_H
