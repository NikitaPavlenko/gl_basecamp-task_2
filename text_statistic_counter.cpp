#include "text_statistic_counter.hpp"
#include <QTextDocument>
#include <QRegularExpression>

TextStatisticCounter::TextStatisticCounter(QTextDocument* document, QObject *parent)
  : QObject(parent)
  , document_(document)
  , word_regex_("(\\s|\\n|\\r)+")
  , word_count_last_(0)
{
  if (document_)
    document_->connect(document_, &QTextDocument::contentsChanged, this, &TextStatisticCounter::slot_ContentsChanged);
}

void TextStatisticCounter::SetDocument(QTextDocument* new_document)
{
  if (new_document)
  {
    if (document_)
    {
      document_->disconnect(document_, &QTextDocument::contentsChanged, this, &TextStatisticCounter::slot_ContentsChanged);
      delete document_;
    }
    document_ = new_document;
    document_->connect(document_, &QTextDocument::contentsChanged, this, &TextStatisticCounter::slot_ContentsChanged);
    slot_ContentsChanged();
  }
}

void TextStatisticCounter::slot_ContentsChanged()
{
  emit signal_CharacterCountChanged(document_->characterCount() - document_->blockCount());
  quint32 new_word_count = document_->toRawText().split(word_regex_, QString::SkipEmptyParts).count();
  if (word_count_last_ != new_word_count)
  {
    emit signal_WordCountChanged(new_word_count);
    word_count_last_ = new_word_count;
  }
}
