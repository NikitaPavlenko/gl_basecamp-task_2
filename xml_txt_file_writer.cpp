#include "xml_txt_file_writer.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QTextDocument>

XmlTxtFileWriter::XmlTxtFileWriter()
  : WriteService()
{

}

QString XmlTxtFileWriter::Write(const QTextDocument& document)
{
  QString file_name = QFileDialog::getSaveFileName(nullptr, "Save file", "",
                                                   "TXT file (*.txt);;XML files (*.xml)");
  if (Write(document, file_name))
    return file_name;
  return QString();
}

bool XmlTxtFileWriter::Write(const QTextDocument& document, const QString& file_name)
{
  if (file_name.isEmpty())
    return false;

  QFile file(file_name);
  if (!file.open(QIODevice::WriteOnly))
  {
    QMessageBox::information(nullptr, "Unable to open file", file.errorString());
    return false;
  }
  file.write(document.toRawText().toUtf8());
  file.close();
  return true;
}
