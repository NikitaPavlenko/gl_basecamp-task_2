#ifndef TEXT_EDITOR_HPP
#define TEXT_EDITOR_HPP

#include <QPlainTextEdit>

#include "search_and_xml_syntax_highlighter.hpp"

class LineNumbersArea;
class QDomDocument;

class TextEditor
    : public QPlainTextEdit
{
    Q_OBJECT
  public:
    explicit TextEditor(QWidget* parent = nullptr);
   ~TextEditor();

    void ShowLineNumbers();
    void HideLineNumbers();
    bool IsLineNumbersVisible();

    void SearchText(const QString& pattern);
    void SetSyntaxMode(SearchAndXmlSyntaxHighlighter::SyntaxMode mode);

    bool ValidateLikeXmlText();

  protected:
    void resizeEvent(QResizeEvent *event) override;

private slots:
    virtual void slot_UpdateLineNumbersAreaWidth(int new_block_count);
    virtual void slot_UpdateLineNumbersArea(const QRect &rect, int dy);

  private:
    QScopedPointer<LineNumbersArea> line_numbers_area_;
    QScopedPointer<SearchAndXmlSyntaxHighlighter> text_searcher_;
    QScopedPointer<QDomDocument> xml_validator_;

    friend class LineNumbersArea;
};

#endif // TEXTEDITOR_HPP
