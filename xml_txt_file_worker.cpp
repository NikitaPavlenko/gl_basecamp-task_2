#include "xml_txt_file_worker.hpp"

#include "xml_txt_file_reader.hpp"
#include "xml_txt_file_writer.hpp"

#include <QMessageBox>


XmlTxtFileWorker::XmlTxtFileWorker(QTextDocument* document)
  : FileService()
  , document_(document)
  , reader_(new XmlTxtFileReader())
  , writer_(new XmlTxtFileWriter())
  , current_file_text_hash_(0)
{

}

bool XmlTxtFileWorker::Create()
{
  if (Close())
    return true;
  return false;
}

bool XmlTxtFileWorker::Open()
{
  if (Close())
  {
    current_file_name_ = reader_->Read(*document_);
    if (current_file_name_.isEmpty())
      return false;

    current_file_text_hash_ = qHash(document_->toRawText());
    return true;
  }
  return false;
}

bool XmlTxtFileWorker::Save()
{
  if (current_file_name_.isEmpty())
    return SaveAs();

  if (qHash(document_->toRawText()) == current_file_text_hash_)
    return true;
  return writer_->Write(*document_, current_file_name_);
}

bool XmlTxtFileWorker::SaveAs()
{
  if (document_)
  {
    current_file_name_ = writer_->Write(*document_);
    if (current_file_name_.isEmpty())
      return false;
    current_file_text_hash_ = qHash(document_->toRawText());
    return true;
  }
  return false;
}

bool XmlTxtFileWorker::Close()
{
  if (document_)
  {
    if (qHash(document_->toRawText()) != current_file_text_hash_)
    {
      QMessageBox::StandardButton msg_box_button = QMessageBox::information(nullptr,
                                                                            "Unsaved changes",
                                                                            "Do you want save data?",
                                                                            QMessageBox::Save |
                                                                            QMessageBox::Close |
                                                                            QMessageBox::Cancel);
      switch(msg_box_button)
      {
        case QMessageBox::Cancel:
          return false;
        case QMessageBox::Save:
          if (!Save())
            return false;
        case QMessageBox::Close:
          break;
        default:
          return false;
      }
    }
    document_->clear();
    current_file_name_ = "";
    current_file_text_hash_ = 0;
  }
  return true;
}
