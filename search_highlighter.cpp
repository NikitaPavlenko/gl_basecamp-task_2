#include "search_highlighter.hpp"
#include <QTextDocument>

SearchHighlighter::SearchHighlighter(QTextDocument* document)
  : QSyntaxHighlighter(document)
  , regex_("")
  , background_color_(Qt::yellow)
  , foreground_color_(Qt::magenta)
{
}

void SearchHighlighter::Search(const QString& text)
{
  regex_.setPattern(text);
  rehighlight();
}

void SearchHighlighter::highlightBlock(const QString& text)
{
  QTextCharFormat current_format;
  QRegularExpressionMatchIterator it = regex_.globalMatch(text);
  while (it.hasNext())
  {
    QRegularExpressionMatch match = it.next();
    current_format = format(match.capturedStart());
    current_format.setBackground(background_color_);
    current_format.setForeground(foreground_color_);
    setFormat(match.capturedStart(), match.capturedLength(), current_format);
  }
}
