#ifndef FILE_SERVICE_H
#define FILE_SERVICE_H

#include <QString>

class FileService
{
  public:
    virtual ~FileService() {}

    virtual bool Create() = 0;
    virtual bool Open() = 0;
    virtual bool Save() = 0;
    virtual bool SaveAs() = 0;
    virtual bool Close() = 0;

    inline QString GetCurrentFileName() const
    {
      return current_file_name_;
    }

  protected:
    QString current_file_name_;
};

#endif // FILE_SERVICE_H
