#ifndef WRITE_SERVICE_H
#define WRITE_SERVICE_H

class QString;

template <typename DataT>
class WriteService
{
  public:
    virtual ~WriteService() {};

    virtual QString Write(const DataT&) = 0;
    virtual bool Write(const DataT&, const QString&) = 0;
};

#endif // WRITE_SERVICE_H
