#ifndef TEXT_STATISTIC_COUNTER_HPP
#define TEXT_STATISTIC_COUNTER_HPP

#include <QObject>
#include <QRegularExpression>

class QTextDocument;

class TextStatisticCounter
    : public QObject
{
    Q_OBJECT
  public:
    explicit TextStatisticCounter(QTextDocument* document, QObject *parent = nullptr);

    void SetDocument(QTextDocument* new_document);
  signals:
    void signal_WordCountChanged(quint32 new_count);
    void signal_CharacterCountChanged(quint32 new_count);

  private slots:
    void slot_ContentsChanged();

  private:
    QTextDocument* document_;
    QRegularExpression word_regex_;
    quint32 word_count_last_;
};

#endif // TEXTSTATISTICCOUNTER_HPP
