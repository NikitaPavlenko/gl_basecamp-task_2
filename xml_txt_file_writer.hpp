#ifndef XML_TXT_FILE_WRITER_H
#define XML_TXT_FILE_WRITER_H

#include "write_service.hpp"

class QString;
class QTextDocument;

class XmlTxtFileWriter : public WriteService<QTextDocument>
{
  public:
    XmlTxtFileWriter();
    ~XmlTxtFileWriter() {}

    QString Write(const QTextDocument& document) override;
    bool Write(const QTextDocument& document, const QString& file_name) override;
};

#endif // XML_TXT_FILE_WRITER_H
