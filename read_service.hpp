#ifndef READ_SERVICE_H
#define READ_SERVICE_H

class QString;

template <typename DataT>
class ReadService
{
  public:
    virtual ~ReadService() {};
    virtual QString Read(DataT&) = 0;
};

#endif // READ_SERVICE_H
