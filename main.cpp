#include "mainwindow.hpp"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MainWindow w;
  w.setWindowTitle("Smart editor");
  w.show();

  return a.exec();
}
