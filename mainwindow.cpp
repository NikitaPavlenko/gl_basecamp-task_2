#include "mainwindow.hpp"

#include <QLineEdit>

#include "text_statistic_counter.hpp"
#include "text_editor.hpp"
#include "xml_txt_file_worker.hpp"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , invalid_xml_text_message_("XML Text is invalid!")
  , valid_xml_text_message_("XML Text is valid!")

{
  ui.setupUi(this);

  search_line_edit_ = new QLineEdit(ui.tool_bar);
  statistic_counter_.reset(new TextStatisticCounter(ui.editor->document(), this));
  file_worker_.reset(new XmlTxtFileWorker(ui.editor->document()));

  Setup();
}

MainWindow::~MainWindow()
{
  delete search_line_edit_;
}

void MainWindow::slot_OpenFile()
{
  if (file_worker_->Open())
  {
    SetEditorEnable();
    if (QRegularExpression("\\.xml$").match(file_worker_->GetCurrentFileName()).hasMatch())
      ui.editor->SetSyntaxMode(SearchAndXmlSyntaxHighlighter::SyntaxMode::kXml);
    else
      ui.editor->SetSyntaxMode(SearchAndXmlSyntaxHighlighter::SyntaxMode::kTxt);
  }
}

void MainWindow::slot_SaveFile()
{
  if (ui.editor->isEnabled())
    file_worker_->Save();
}

void MainWindow::slot_SaveAsFile()
{
  if (ui.editor->isEnabled())
    file_worker_->SaveAs();
}

void MainWindow::slot_CloseFile()
{
  if (file_worker_->Close())
    ui.editor->setEnabled(false);
}

void MainWindow::slot_CreateTxtFile()
{
  if (file_worker_->Create())
  {
    SetEditorEnable();
    ui.editor->SetSyntaxMode(SearchAndXmlSyntaxHighlighter::SyntaxMode::kTxt);
  }
}

void MainWindow::slot_CreateXmlFile()
{
  if (file_worker_->Create())
  {
    SetEditorEnable();
    ui.editor->SetSyntaxMode(SearchAndXmlSyntaxHighlighter::SyntaxMode::kXml);
  }
}

void MainWindow::slot_CutText()
{
  ui.editor->cut();
}

void MainWindow::slot_CopyText()
{
  ui.editor->copy();
}

void MainWindow::slot_PasteText()
{
  ui.editor->paste();
}

void MainWindow::slot_RedoAction()
{
  ui.editor->redo();
}

void MainWindow::slot_UndoAction()
{
  ui.editor->undo();
}

void MainWindow::slot_ShowHideLineNumbers()
{
  if (ui.editor->IsLineNumbersVisible())
    ui.editor->HideLineNumbers();
  else
    ui.editor->ShowLineNumbers();
}

void MainWindow::slot_XmlFileValidate()
{
  if (ui.editor->isEnabled() && ui.editor->toPlainText().size() != 0)
  {
    if (ui.editor->ValidateLikeXmlText())
      ui.xml_validaion_result_label->setText(valid_xml_text_message_);
    else
      ui.xml_validaion_result_label->setText(invalid_xml_text_message_);
  }
}

void MainWindow::slot_SearchPatternChanged()
{
  if (ui.editor->isEnabled())
    ui.editor->SearchText(search_line_edit_->text());
}

void MainWindow::slot_WordCountChanged(quint32 new_count)
{
  ui.word_count_number_label->setText(QString::number(new_count));
}

void MainWindow::slot_CharacterCountChanged(quint32 new_count)
{
  ui.character_count_number_label->setText(QString::number(new_count));
}

void MainWindow::slot_ClearXmlValidateResult()
{
  ui.xml_validaion_result_label->clear();
}

void MainWindow::Setup()
{
  ui.editor_layout->addWidget(ui.editor);
  ui.editor->setFont(QFont(ui.editor->font().family(), 14));
  ui.editor->setEnabled(false);
  ui.editor->HideLineNumbers();

  search_line_edit_->setFixedWidth(150);
  ui.tool_bar->addWidget(search_line_edit_);

  //Statistic connections
  connect(&*statistic_counter_, &TextStatisticCounter::signal_CharacterCountChanged,
          this, &MainWindow::slot_CharacterCountChanged);
  connect(&*statistic_counter_, &TextStatisticCounter::signal_WordCountChanged,
          this, &MainWindow::slot_WordCountChanged);
  //Clear validation xml result
  connect(ui.editor, &TextEditor::textChanged,
          this, &MainWindow::slot_ClearXmlValidateResult);
  //Action connections
  connect(ui.new_txt_file_action, &QAction::triggered,
          this, &MainWindow::slot_CreateTxtFile);
  connect(ui.new_xml_file_action, &QAction::triggered,
          this, &MainWindow::slot_CreateXmlFile);
  connect(ui.save_file_action, &QAction::triggered,
          this, &MainWindow::slot_SaveFile);
  connect(ui.save_as_file_action, &QAction::triggered,
          this, &MainWindow::slot_SaveAsFile);
  connect(ui.close_file_action, &QAction::triggered,
          this, &MainWindow::slot_CloseFile);
  connect(ui.open_file_action, &QAction::triggered,
          this, &MainWindow::slot_OpenFile);
  connect(ui.cut_text_action, &QAction::triggered,
          this, &MainWindow::slot_CutText);
  connect(ui.copy_text_action, &QAction::triggered,
          this, &MainWindow::slot_CopyText);
  connect(ui.paste_text_action, &QAction::triggered,
          this, &MainWindow::slot_PasteText);
  connect(ui.redo_action, &QAction::triggered,
          this, &MainWindow::slot_RedoAction);
  connect(ui.undo_action, &QAction::triggered,
          this, &MainWindow::slot_UndoAction);
  connect(ui.show_hide_line_numbers_action, &QAction::triggered,
          this, &MainWindow::slot_ShowHideLineNumbers);
  connect(ui.xml_validation_action, &QAction::triggered,
          this, &MainWindow::slot_XmlFileValidate);
  connect(search_line_edit_, &QLineEdit::textChanged,
          this, &MainWindow::slot_SearchPatternChanged);
}

void MainWindow::SetEditorEnable()
{
  ui.editor->setEnabled(true);
  ui.editor->ShowLineNumbers();
  search_line_edit_->clear();
}
