#ifndef LINE_NUMBERS_AREA_HPP
#define LINE_NUMBERS_AREA_HPP

#include <QWidget>

class TextEditor;

class LineNumbersArea : public QWidget
{
    Q_OBJECT
  public:
    explicit LineNumbersArea(TextEditor *editor);

    QSize sizeHint() const override;
    int UpdateWidth() const;

    // QWidget interface
  protected:
    void paintEvent(QPaintEvent* event) override;

  private:
    TextEditor* editor_;
    qint16 left_padding_;
    qint16 right_padding_;
};

#endif // LINENUMBERSAREA_HPP
