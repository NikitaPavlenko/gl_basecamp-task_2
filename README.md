implemented:
	- Support standard text editing commands (copy, move, back / front actions history).
    - Read .TXT and .XML files.
    - Support standard file actions (open, close, create, save, save as).
    - Show line numbers.
        1. configurable -> user can enable/disable it.
    - Support UTF-8 text.
    - Support search in file.
        1. Also support with using regexps.
    - Show statistics (number of characters, words, etc).
    - Show formatted XML text 
        1. Show tags/values/attributes with different colours
        2. Ability to make XML validation
    - Compile for Linux and Windows.
	- Non-blocking UI – it should not freeze while using the app. 
    - Documentation: requirements specification, software design documentation (application modules description, UML diagrams).

Not implemented:
	Ability to shrink/expand the content of XML nodes.
	
Warnings:
  If load very huge file, can freeze. 